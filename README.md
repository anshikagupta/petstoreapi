# PetStoreAPI Project

PetStoreAPI project test petstore swagger rest API which includes PET, STORE and USERS resources and their respective HTTP methods. This project tests all the API resources and validates their positive and negative scenarios. After the test completes it generates Junit and Serenity aggregation reports.

## Tools Used

* Maven
* Cucumber
* JUnit
* Serenity BDD framework
* JAVA
* Gitlab as CI platform

## Requirements

In order to utilise this project you need to have the following softwares installed:

* Apache Maven 3.6.3
* Java 1.8
* GitLab Account to run the project in CI platform.
* Pet Store API server - https://petstore.swagger.io/
* Eclipse editor

In order to run the project in GitLab you can use the docker image containing Apache maven and jdk 1.8( example docker image: rvancea/maven-chrome-jdk8:latest )

## Installation On Local Machine

Pre-installation software:
Before installing the actual PetStoreAPI project one should have below softwares.

Apache Maven Build Tool.
JAVA Jdk1.8
Eclipse 

Follow below steps to install the PetStoreAPI project locally.

* Download the PetStoreAPI project from the GitLab link.
https://gitlab.com/anshikagupta/petstoreapi.git


* Unzip the project and import the project into Eclipse.

File --> Import --> Existing Maven Projects --> Next --> Browse the downloaded project--> Finish

## Running on Local Machine :

Now right click on the project and navigate as follows 
Run As --> Run Configurations

Click on Maven Build --> Create new configuration

Enter Any name to the new configuration --> enter goals to run (clean verify serenity:aggregate ) --> click Run button

After clicking on the run button maven start running the clean, verify and aggregate goals.

**Clean :**
Clean maven phase will clean the target folder of the project.

**Verify :**
Verify maven phase will run the maven failsafe plugin which will run all the scenarios mentioned in the cucumber feature files. After that this will also generate reports for each individual scenario.

**Serenity:aggregate :** 
It will generate the aggregate report target/site/serenity/index.html

## Running in GitLab CI Server :

In order to run the project in GitLab CI Server you need to follow the below steps.


- Login to the GitLab account.

- Go to the below public GitLab repository where the project is uploaded.
https://gitlab.com/anshikagupta/petstoreapi.git


- Here README.md file is created with the details of the project.

- GitLab CI pipeline file (.gitlab-ci.yml) created to run the project and associated test report with GitLab job to view or download.
    Click on the CI/CD tab in the left panel.
    CI/CD --> Pipelines --> Run Pipeline → Again click on Run Pipeline

This will run the Pipeline which has a Test job. The Test Job will perform all the tests mentioned in the feature files and generate aggregate reports. Which will be then available for view or download.










