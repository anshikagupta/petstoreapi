package apis.petstoreapi.stepdefinitions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import apis.petstoreapi.actions.AddListUsers_Action;
import apis.petstoreapi.actions.CreateUser_Action;
import apis.petstoreapi.actions.DeleteUser_Action;
import apis.petstoreapi.actions.FindByUsername_Action;
import apis.petstoreapi.actions.LoginUser_Action;
import apis.petstoreapi.actions.LogoutUser_Action;
import apis.petstoreapi.actions.UpdateUser_Action;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class User_StepDefinition extends BaseStepDefinition{
	
	@Steps
	AddListUsers_Action addlistAction;
	
	@Steps
	FindByUsername_Action findbyuserAction;
	
	@Steps
	UpdateUser_Action updateuserAction;
	
	@Steps
	LoginUser_Action loginuserAction;
	
	@Steps
	CreateUser_Action createuserAction;
	
	@Steps
	DeleteUser_Action deleteuserAction;
	
	@Steps
	LogoutUser_Action logoutuserAction;
	
	@When("Create user with given {string} and {string}")
	public void create_user_with_given_and(String userId,String userName) {
	    
		response = addlistAction.addListUsers(userId,userName);
	}

	@Then("Response body should have {string} and {string} pair")
	public void response_should_have_message(String key,String value) {
	    
		response.then().body(key, equalTo(value));
		
	}

	@When("User query to find {string}")
	public void user_query_to_find(String username) {
		
		response=findbyuserAction.findByUsername(username);
		
	}

	@When("User update {string} to {string} for {string} and {string}")
	public void user_update_for(String lastname,String newLastName,String userName,String userId) {
	    
		response=updateuserAction.updateUser(lastname,newLastName,userName,userId);
	}

	@When("User login with {string} and {string}")
	public void user_login_with_and(String username, String password) {
	   
		response= loginuserAction.loginUser(username, password);
	}

	@When("User create {string} with {string} while logged in")
	public void user_create_while_logged_in(String new_userName,String new_userId) {
		
	    response=createuserAction.createNewUser(new_userName,new_userId);
	}

	@When("User delete {string} while logged in")
	public void user_delete_while_logged_in(String username) {
		
	    response= deleteuserAction.deleteUser(username);
	}


	@When("User logout from the system")
	public void user_logout_from_the_system() {
	    response= logoutuserAction.logoutUser();
	}
}
