package apis.petstoreapi.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;

import apis.petstoreapi.actions.DeleteOrder_Action;
import apis.petstoreapi.actions.FindByOrderId_Action;
import apis.petstoreapi.actions.PetInventory_Action;
import apis.petstoreapi.actions.PlaceOrderPet_Action;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Store_StepDefinition extends BaseStepDefinition{
	
	@Steps
	PlaceOrderPet_Action placeOrder;
	
	@Steps
	FindByOrderId_Action findOrderAction;
	
	@Steps
	DeleteOrder_Action deleteOrderAction;
	
	@Steps
	PetInventory_Action petInvenoryAction;
	
	@When("User place order for purchasing the pet with {string} and {string}")
	public void user_place_order_for_purchasing_the_pet_with(String orderId,String petId) {
		
		response = placeOrder.placeOrderPet(orderId,petId);
		
	}
	
	@When("User place order for purchasing the pet with invalid order data")
	public void user_place_order_for_purchasing_the_pet_with_invalid_order_data() {
		
		response = placeOrder.placeInvalidOrder();
		
	}

	@Then("Response body should have matching {string} and {string}")
	public void response_should_have_matching_and(String orderId,String petId) {
		
		response.then().body("id", equalTo(Integer.parseInt(orderId))).body("petId", equalTo(Integer.parseInt(petId)));
	}
	
	@When("User find purchase order by {string}")
	public void user_find_purchase_by(String orderId) {
	    
		response=findOrderAction.findByID(orderId);
		
	}
	
	@When("User deletes the order with {string}")
	public void user_deletes_the(String orderId) {
	    
		response=deleteOrderAction.deleteOrder(orderId);
		
	}

	@Then("Response body should have deleted orderId {string}")
	public void response_should_have_deleted_orderId(String orderId) {
		
		response.then().body("message", equalTo(orderId));
	}
	
	@When("User query pet inventory")
	public void user_query_pet_inventory() {
	    
		response=petInvenoryAction.getPetInventory();
		
	}
}
