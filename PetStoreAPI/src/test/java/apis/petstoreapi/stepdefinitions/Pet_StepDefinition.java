package apis.petstoreapi.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;

import apis.petstoreapi.actions.AddNewPet_Action;
import apis.petstoreapi.actions.Base_Action;
import apis.petstoreapi.actions.DeletePet_Action;
import apis.petstoreapi.actions.FindByIDPet_Action;
import apis.petstoreapi.actions.FindByStatusPet_Action;
import apis.petstoreapi.actions.UpdateNewPet_Action;
import apis.petstoreapi.actions.updateNewPetFormData_Action;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class Pet_StepDefinition extends BaseStepDefinition{
	
	@Steps
	AddNewPet_Action addPetAction;
	
	@Steps
	UpdateNewPet_Action updatePetAction;
	
	@Steps
	Base_Action baseAction;
	
	@Steps
	FindByStatusPet_Action findByStatusAction;
	
	@Steps
	FindByIDPet_Action findByIDAction;
	
	@Steps
	updateNewPetFormData_Action updateFormDataAction;
	
	@Steps
	DeletePet_Action deletePet;
	
	
	@Given("User has {string} of Rest API with {string}")
	public void user_has_of_rest_api_with(String baseURI, String resourcePath) {
		
		baseAction.decorateURL(baseURI, resourcePath);
	}

	@When("User add a new pet to the store with {string} and {string} and {string}")
	public void user_add_a_new_pet_to_the_store_with_and_and(String petID, String petName,String petStatus) {
		
		response = addPetAction.addNewPet(petID,petName,petStatus);
	}
	
	@Then("Response should have status code {string}")
	public void response_should_have_status_code(String status_code) {
		
		response.then().statusCode(Integer.parseInt(status_code));
		
	}
	
	@Then("Response body should have matching {string} and {string} and {string}")
	public void response_body_should_have_matching(String petId,String petName,String petStatus) {
		
		response.then().body("id", equalTo(Integer.parseInt(petId))).body("name", equalTo(petName)).body("status", equalTo(petStatus));
	}
	
	@Then("Response body should have error message {string}")
	public void response_body_should_have_error_message(String errorMsg) {
		
		response.then().body("message", equalTo(errorMsg));
	}
	
	@When("User add a new pet to the store with invalid petId")
	public void user_add_a_new_pet_to_the_store_with_invalid_petId() {
		
		response = addPetAction.addNewInvalidPet();
	}

	@When("User update pet name to {string} for {string} petId and {string}")
	public void user_update_pet_name_to_for_pet_id(String petName, String petId,String petStatus) {
		
		response=updatePetAction.updateNewPet(petName, petId,petStatus);
	}
	
	@When("User query pets with {string} petStatus in petstore")
	public void user_query_pets_with_petStatus_in_petstore(String petStatus) {
		
		response=findByStatusAction.findByStatusPet(petStatus);
		
	}
	
	@Then("Response body should have available pet status {string}")
	public void respoonse_body_should_have_available_pet_status(String petStatus) {
	    
		response.then().body("[0].status", equalTo(petStatus));
		
	}
	
	@When("User query with {string} petId in petstore")
	public void user_query_with_pet_id_in_petstore(String petId) {
	   
		response=findByIDAction.findByIDPet(petId);
	}

	@When("User update pet status to {string} for {string} and {string}")
	public void user_update_pet_status_to_for(String petStatus, String petId,String petName) {
	    
		response=updateFormDataAction.updateNewPetFormData(petStatus, petId,petName);
	}

	@When("User delete {string} Pet Id from petstore using {string}")
	public void user_delete_pet_id_from_petstore(String petId,String api_key) {
		
		response=deletePet.deletePet(petId, api_key);
	}
	
}
