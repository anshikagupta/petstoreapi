package apis.petstoreapi.constants;

public class Constant {
	
	//Test Runner constants
	public static final String FEATURE_PACKAGE_PATH="src/test/resources/features/petstoreapi/";
	public static final String STEP_DEFINITION_PKG_NAME="apis.petstoreapi.stepdefinitions";
	
	//http header constants
	public static final String CONTENT_TYPE="application/json";
	public static final String UTF8="UTF-8";
	
	
}
