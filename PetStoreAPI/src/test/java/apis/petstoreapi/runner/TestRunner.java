package apis.petstoreapi.runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import static apis.petstoreapi.constants.Constant.*;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features=FEATURE_PACKAGE_PATH,
		glue= {STEP_DEFINITION_PKG_NAME}
		
)
public class TestRunner {

}
