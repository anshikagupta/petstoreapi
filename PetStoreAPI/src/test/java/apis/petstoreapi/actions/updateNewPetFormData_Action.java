package apis.petstoreapi.actions;

import java.util.HashMap;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class updateNewPetFormData_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response updateNewPetFormData(String petStatus, String petId,String petName) 
	{
		HashMap<String, String> formdata=new HashMap<String, String>();
		formdata.put("status", petStatus);
		formdata.put("name", petName);
		
		response=SerenityRest.given().formParams(formdata).post(url+petId);
		return response;
		
	}
	
}
