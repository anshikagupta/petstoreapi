package apis.petstoreapi.actions;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DeleteOrder_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response deleteOrder(String orderId) 
	{
		response=SerenityRest.given().delete(url + orderId);
		return response;
		
	}
	
}
