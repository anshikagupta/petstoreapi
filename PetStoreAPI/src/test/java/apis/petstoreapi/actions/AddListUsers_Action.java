package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import java.util.ArrayList;

import apis.petstoreapi.models.User;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class AddListUsers_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response addListUsers(String userId,String userName) 
	{
		User user=new User();
		user.setId(Integer.parseInt(userId));
		user.setEmail(userName);
		user.setFirstName(userName);
		user.setLastName(userName);
		user.setPassword(userName);
		user.setPhone(userName);
		user.setUsername(userName);
		user.setUserStatus(Integer.parseInt(userId));
		
		ArrayList<User> listUsers=new ArrayList<User>(); 
		listUsers.add(user);
		
		response=SerenityRest.given().contentType(CONTENT_TYPE).body(listUsers).post(url);
		return response;
		
	}
	
}
