package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import apis.petstoreapi.models.User;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class UpdateUser_Action extends Base_Action{
	
	Response response;
	
	@Step
	public Response updateUser(String lastname,String newLastName,String userName,String userId) 
	{
		
		User user=new User();
		user.setId(Integer.parseInt(userId));
		user.setEmail(userName);
		user.setFirstName(userName);
		user.setLastName(newLastName);
		user.setPassword(userName);
		user.setPhone(userName);
		user.setUsername(userName);
		user.setUserStatus(Integer.parseInt(userId));
		
		response=SerenityRest.given().contentType(CONTENT_TYPE).body(user).put(url+userName);
		return response;
		
	}
	
}
