package apis.petstoreapi.actions;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class LogoutUser_Action extends Base_Action{
	
Response response;
	
	@Step
	public Response logoutUser()
	{
		response=SerenityRest.given().get(url);
		return response;
		
	}

}
