package apis.petstoreapi.actions;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DeleteUser_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response deleteUser(String username) 
	{
		response=SerenityRest.given().delete(url + username);
		return response;
		
	}
	
}
