package apis.petstoreapi.actions;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class FindByOrderId_Action extends Base_Action{
	
Response response;
	
	@Step
	public Response findByID(String orderId) 
	{
		response=SerenityRest.given().get(url+orderId);
		return response;
	}

}
