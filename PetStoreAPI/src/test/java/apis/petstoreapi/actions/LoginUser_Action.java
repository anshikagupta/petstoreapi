package apis.petstoreapi.actions;
import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class LoginUser_Action extends Base_Action{
	
	Response response;
	
	@Step
	public Response loginUser(String username,String password) 
	{
		response=SerenityRest.given().contentType(CONTENT_TYPE).get(url+ "?username="+username+"&password="+password);
		return response;
		
	}
	
}
