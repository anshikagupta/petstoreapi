package apis.petstoreapi.actions;

import net.thucydides.core.annotations.Step;

public class Base_Action {
	
	static String url;
	int statusCode;
	
	@Step
	public void decorateURL(String baseURI,String resourcePath) {
		
		url= baseURI+resourcePath;
	}
}
