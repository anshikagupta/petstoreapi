package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import apis.petstoreapi.models.PlaceOrder;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class PlaceOrderPet_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response placeOrderPet(String orderId,String petId) 
	{
		PlaceOrder order=new PlaceOrder();
		order.setId(Integer.parseInt(orderId));
		order.setPetId(Integer.parseInt(petId));
		order.setStatus("placed");
		order.setComplete(true);
		
		response=SerenityRest.given().contentType(CONTENT_TYPE).body(order).post(url);
		
		return response;
	}
	
	@Step
	public Response placeInvalidOrder() 
	{
		response=SerenityRest.given().contentType(CONTENT_TYPE).body("").post(url);
		return response;
		
	}
	
}
