package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import java.util.ArrayList;
import java.util.List;

import apis.petstoreapi.models.Category;
import apis.petstoreapi.models.CreatePet;
import apis.petstoreapi.models.Tag;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class UpdateNewPet_Action extends Base_Action{
	
	Response response;
	
	@Step
	public Response updateNewPet(String petName,String petId,String petStatus) 
	{
		
		//create JSON for updating existing pet with new petName
		Category category=new Category();
		category.setId(Integer.parseInt(petId));
		
		Tag tag=new Tag();
		tag.setId(Integer.parseInt(petId));
		
		CreatePet createPet=new CreatePet();
		createPet.setId(Integer.parseInt(petId));
		createPet.setCategory(category);
		List<Tag> listTag=new ArrayList<Tag>();
		listTag.add(tag);
		createPet.setTags(listTag);
		createPet.setName(petName);
		createPet.setStatus(petStatus);
		
		response=SerenityRest.given().contentType(CONTENT_TYPE).body(createPet).put(url);
		
		return response;
		
	}
	
}
