package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import apis.petstoreapi.models.User;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class CreateUser_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response createNewUser(String new_userName,String new_userId) 
	{
		User user=new User();
		user.setId(Integer.parseInt(new_userId));
		user.setEmail(new_userName);
		user.setFirstName(new_userName);
		user.setLastName(new_userName);
		user.setPassword(new_userName);
		user.setPhone(new_userName);
		user.setUsername(new_userName);
		user.setUserStatus(Integer.parseInt(new_userId));
		
		response=SerenityRest.given().contentType(CONTENT_TYPE).body(user).post(url);
		return response;
		
	}
	
}
