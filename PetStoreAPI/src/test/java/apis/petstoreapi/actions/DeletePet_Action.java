package apis.petstoreapi.actions;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DeletePet_Action extends Base_Action {
	
	Response response;
	
	@Step
	public Response deletePet(String petId,String api_key) 
	{
		response=SerenityRest.given().header("api_key", api_key).delete(url + petId);
		return response;
		
	}
	
}
