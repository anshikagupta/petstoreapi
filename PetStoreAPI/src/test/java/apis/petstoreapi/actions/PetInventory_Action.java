package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class PetInventory_Action extends Base_Action{
	
	Response response;
	
	@Step
	public Response getPetInventory() 
	{
		response=SerenityRest.given().contentType(CONTENT_TYPE).get(url);
		return response;
		
	}
	
}
