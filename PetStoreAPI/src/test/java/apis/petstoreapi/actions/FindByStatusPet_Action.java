package apis.petstoreapi.actions;

import static apis.petstoreapi.constants.Constant.CONTENT_TYPE;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class FindByStatusPet_Action extends Base_Action{
	
	Response response;
	
	@Step
	public Response findByStatusPet(String petStatus) 
	{
		response=SerenityRest.given().contentType(CONTENT_TYPE).get(url+"?status="+petStatus);
		return response;
		
	}
	
}
