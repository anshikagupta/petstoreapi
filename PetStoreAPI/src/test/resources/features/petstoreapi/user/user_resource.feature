Feature: Operations about user


Scenario Outline: Creates list of users with given input array (POST /user/createWithArray)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When Create user with given '<valid_userId>' and '<valid_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath     			| valid_userId | valid_userName  | response_key | response_value | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/createWithList		| 101  	 			 |	user1 	 			 | message 			| ok 						 | 200 						 |
	

Scenario Outline: Creates list of users with given input array (POST /user/createWithList)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When Create user with given '<valid_userId>' and '<valid_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath     			| valid_userId | valid_userName  | response_key | response_value | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/createWithList		| 102  	 			 |	user2 	 			 | message 			| ok 						 | 200 						 |


Scenario Outline: Get user data with valid user name (GET /user/{username})
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<valid_userId>' and '<valid_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query to find '<valid_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | valid_userId | valid_userName |	response_key  | response_value | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/		  	| 103 	 			 | user3	   			|	username	    | user3		  		 | 200 						 |
	
@Invalid_user_case
Scenario Outline: Get user data with invalid user name (GET /user/{username})
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query to find '<invalid_userName>'
Then Response should have status code '<400_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | invalid_userName	|	response_key | response_value 					 | 400_status_code |
	|	https://petstore.swagger.io/v2 | /user/		  	| 	%   						|	message 		 | Invalid username supplied | 400						 |


Scenario Outline: Get user data with non-existing user name (GET /user/{username})
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query to find '<user_not_register>'
Then Response should have status code '<404_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | response_key |	response_value   | user_not_register | 404_status_code |
	|	https://petstore.swagger.io/v2 | /user/		  	| message	   	 |	User not found 	 | 	 user999		  	 | 404 						 |


Scenario Outline: Logs user into the system with valid data (GET /user/login )
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<valid_userId>' and '<valid_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User login with '<valid_userName>' and '<valid_password>'
Then Response should have status code '<200_status_code>'
Examples: 
	|	baseURI 					   					 | resourcePath | valid_userId |	valid_userName |	valid_password | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/login  | 104 	 			 |	user4		   		 |	user4			 	   | 200						 |

@Invalid_user_case
Scenario Outline: Logs user into the system with invalid data (GET /user/login )
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User login with '<invalid_userName>' and '<invalid_password>'
Then Response should have status code '<400_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | invalid_userName |	invalid_password | response_key | response_value 										 | 400_status_code |
	|	https://petstore.swagger.io/v2 | /user/login  | 	%	   		 			 |				 	   			 | message			| Invalid username/password supplied | 400						 |


Scenario Outline: Logged in user update a valid user data (PUT /user/{username})
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_userName>' and '<loggedin_password>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update '<oldLastName>' to '<newLastName>' for '<loggedin_userName>' and '<loggedin_userId>'
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_userName |	loggedin_password | oldLastName | newLastName | response_key | response_value | 200_status_code|
	|	https://petstore.swagger.io/v2 | /user/		  	| 105		 					|	user5	   			 		|		user5	      		| user5 	 		| user_new 		| message 		 | 105						| 200   				 |

@Invalid_user_case
Scenario Outline: Logged in user update a invalid user data (PUT /user/{username})
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_userName>' and '<loggedin_password>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update '<oldLastName>' to '<newLastName>' for '<invalid_userName>' and '<invalid_userId>'
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_userName |	loggedin_password  | oldLastName | newLastName | response_key | response_value |invalid_userName | invalid_userId | 400_status_code| 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/		  	| 106		 					|	user6	   			 		|		user6	      		 | user6 	 		 | user_new 	 | message 		 	| bad input			 | 		%						 |		001					| 400   				 | 200 						 |


@Invalid_user_case
Scenario Outline: Logged in and Updated user with non-existing user data (PUT /user/{username})
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_userName>'
Then Response should have status code '200'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_userName>' and '<loggedin_password>'
Then Response should have status code '200'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update '<oldLastName>' to '<newLastName>' for '<user_not_registered>' and '<userId_not_registered>'
Then Response should have status code '404'
And Response body should have 'message' and 'User not found' pair
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_userName |	loggedin_password | oldLastName | newLastName | response_key | user_not_registered | userId_not_registered | 
	|	https://petstore.swagger.io/v2 | /user/		  	| 107		 					|	user7	   			 		|		user7	      		| user7 	 		| user_new 		| message 		 | user999 						 | 999							 		 | 


Scenario Outline: User Logged in and create another new user (POST /user )
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_userName>'
Then Response should have status code '200'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_userName>' and '<loggedin_password>'
Then Response should have status code '200'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User create '<new_userName>' with '<new_userId>' while logged in
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_userName |	loggedin_password | new_userName | new_userId | response_key | response_value | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user		  	| 108		 					|	user8	   			 		|		user8	      		| user9 			 | 109 			 	|	message 		 | 109			 			| 200 						|


Scenario Outline: Delete a vaid user by logged in usser ( DELETE /user/{username} )
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_username>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_username>' and '<loggedin_password>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<new_userId>' and '<new_userName>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User delete '<new_userName>' while logged in
Then Response should have status code '<200_status_code>'
And Response body should have '<response_key>' and '<response_value>' pair
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_username |	loggedin_password | lastName | new_userName | new_userId | response_key | response_value | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/		  	| 110		 					|	user10	   			 	|	user10	      		| user10 	 | user11 			| 111 			 | message 		 	| user11			 	 | 200 						 |


@Invalid_user_case
Scenario Outline: Delete a invaid user while logged in ( DELETE /user/{username} )
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_username>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_username>' and '<loggedin_password>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User delete '<new_invalid_user>' while logged in
Then Response should have status code '<400_status_code>'
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_username |	loggedin_password | new_invalid_user | 200_status_code | 400_status_code |
	|	https://petstore.swagger.io/v2 | /user		  	| 112		 					|	user12	   			 	|		user12	      	| 	%		 					 | 200 						 | 400						 |


Scenario Outline: Delete a non-existing user while logged in ( DELETE /user/{username} )
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_username>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_username>' and '<loggedin_password>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User delete '<new_user_not_registered>' while logged in
Then Response should have status code '<404_status_code>'
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_username |	loggedin_password | new_user_not_registered | 200_status_code | 404_status_code |
	|	https://petstore.swagger.io/v2 | /user		  	| 113		 					|	user13	   			 	|		user13	      	| user14 									| 200 						| 404							|



Scenario Outline: Logout user from the system (GET /user/logout )
Given User has '<baseURI>' of Rest API with '/user/createWithArray'
When Create user with given '<loggedin_userId>' and '<loggedin_username>'
Then Response should have status code '<200_status_code>'
And Response body should have 'message' and 'ok' pair

Given User has '<baseURI>' of Rest API with '/user/login'
When User login with '<loggedin_username>' and '<loggedin_password>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User logout from the system
Then Response should have status code '<200_status_code>'
Examples: 
	|	baseURI 					   					 | resourcePath | loggedin_userId |	loggedin_username |	loggedin_password | 200_status_code |
	|	https://petstore.swagger.io/v2 | /user/logout	| 115		 					|	user15	   			 	|	user15	      		| 200 						|





