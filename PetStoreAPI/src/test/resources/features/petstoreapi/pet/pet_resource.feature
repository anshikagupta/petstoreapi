Feature: Everything about your Pets


Scenario Outline: Add new pet to the store with valid data (POST /pet)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User add a new pet to the store with '<valid_petId>' and '<petName>' and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'
Examples: 
	|	baseURI 					   					 | resourcePath | valid_petId | petName | available_pet_status | 200_status_code | 
	|	https://petstore.swagger.io/v2 | /pet		  		| 100001			|	doggie 	| available	 					 | 200						 |	


Scenario Outline: Add new pet to the store with Invalid data (POST /pet)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User add a new pet to the store with invalid petId
Then Response should have status code '<405_status_code>'
And Response body should have error message '<invalid_input_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath |	invalid_input_error_msg | 405_status_code | 
	|	https://petstore.swagger.io/v2 | /pet		  		|	no data									| 405 						| 


Scenario Outline: Update an existing pet with valid data (PUT /pet)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User add a new pet to the store with '<valid_petId>' and '<petName>' and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'

When User update pet name to '<new_petName>' for '<valid_petId>' petId and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<new_petName>' and '<available_pet_status>'
Examples: 
	|	baseURI 					   					 | resourcePath |	valid_petId | petName | new_petName  | available_pet_status | 200_status_code | 
	|	https://petstore.swagger.io/v2 | /pet		  		|	100002			|	doggie 	| parrot 		 	 | available	 					| 200							|	

@Invalid_user_case
Scenario Outline: Update an existing pet with Invalid input data (PUT /pet)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update pet name to '<new_petName>' for '<invalid_petId>' petId and '<available_pet_status>'
Then Response should have status code '<400_status_code>'
And Response body should have error message '<invalid_input_error_msg>'
Examples:
	|	baseURI 					   					 | resourcePath |invalid_petId | new_petName | available_pet_status | invalid_input_error_msg | 400_status_code | 
	|	https://petstore.swagger.io/v2 | /pet		  		| 001			   	 | parrot			 | available 						| Invalid ID supplied		 	| 400						 	| 

@Invalid_user_case
Scenario Outline: Update an non-existing pet with valid input data (PUT /pet)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update pet name to '<new_petName>' for '<petId_not_in_store>' petId and '<available_pet_status>'
Then Response should have status code '<404_status_code>'
And Response body should have error message '<pet_not_found_error_msg>'
Examples:
	|	baseURI 					   					 | resourcePath | petId_not_in_store | new_petName | available_pet_status | pet_not_found_error_msg | 404_status_code | 
	|	https://petstore.swagger.io/v2 | /pet		  		| 999 			   		 	 | parrot			 | available 						| Pet not found		 			  | 404						  | 

@Invalid_user_case
Scenario Outline: Update an existing pet with non-valid data (PUT /pet)
When User update pet name to '<new_petName>' for '<invalid_petId>' petId and '<available_pet_status>'
Then Response should have status code '<405_status_code>'
And Response body should have error message '<validation_exception_error_msg>'
Examples:
	|	baseURI 					   					 | resourcePath | invalid_petId | new_petName | available_pet_status | validation_exception_error_msg | 405_status_code |
	|	https://petstore.swagger.io/v2 | /pet		  		|  001		   	 	| parrot			| available 					 | Validation exceptino		 			  | 405						  | 


Scenario Outline: Find pets by available status (GET /pet/findByStatus)
Given User has '<baseURI>' of Rest API with '/pet'
When User add a new pet to the store with '<valid_petId>' and '<petName>' and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query pets with '<available_pet_status>' petStatus in petstore
Then Response should have status code '<200_status_code>'
And Response body should have available pet status '<available_pet_status>'
Examples: 
	|	baseURI 					   					 | resourcePath 		 | valid_petId | petName | available_pet_status | 200_status_code | 
	|	https://petstore.swagger.io/v2 | /pet/findByStatus | 100003			 |	doggie | available	 					| 200							|	

@Invalid_user_case
Scenario Outline: Find pets by invalid status (GET /pet/findByStatus)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query pets with '<invalid_petStatus>' petStatus in petstore
Then Response should have status code '<400_status_code>'
And Response body should have error message '<invalid_status_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath 			| invalid_petStatus | invalid_status_error_msg | 400_status_code |
	|	https://petstore.swagger.io/v2 | /pet/findByStatus	| 				 				  | Invalid status value 		 | 400						 |	


Scenario Outline: Find pet in petstore with valid petId data (GET /pet/{petId} )
Given User has '<baseURI>' of Rest API with '/pet'
When User add a new pet to the store with '<valid_petId>' and '<petName>' and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query with '<valid_petId>' petId in petstore
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'
Examples: 
	|	baseURI 					   					 | resourcePath |	valid_petId | petName | available_pet_status | 200_status_code | 
	|	https://petstore.swagger.io/v2 | /pet/		  	|	100004			|	doggie 	| available	 					 | 200						 |	

@Invalid_user_case
Scenario Outline: Find pet in petstore with invalid petId data (GET /pet/{petId} )
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query with '<invalid_petId>' petId in petstore
Then Response should have status code '<400_status_code>'
And Response body should have error message '<invalid_petId_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath |	invalid_petId | invalid_petId_error_msg | 400_status_code | 
	|	https://petstore.swagger.io/v2 | /pet/		  	|		001					|	Invalid ID supplied	 		| 400							|	


Scenario Outline: Find non-existing pet in petstore with valid petId data (GET /pet/{petId} )
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query with '<pet_id_not_in_store>' petId in petstore
Then Response should have status code '<404_status_code>'
And Response body should have error message '<pet_not_found_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath |	pet_id_not_in_store | pet_not_found_error_msg | 404_status_code | 
	|	https://petstore.swagger.io/v2 | /pet/		  	|	12									|	Pet not found 					| 404							|	


Scenario Outline: Updates a pet in the store with valid petId form data ( POST /pet/{petId} )
Given User has '<baseURI>' of Rest API with '/pet'
When User add a new pet to the store with '<valid_petId>' and '<petName>' and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update pet status to '<new_petStatus>' for '<valid_petId>' and '<petName>'
Then Response should have status code '<200_status_code>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query with '<valid_petId>' petId in petstore
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<new_petStatus>'

Examples: 
	|	baseURI 					   					 | resourcePath |	valid_petId |	available_pet_status | new_petStatus | petName | 200_status_code |
	|	https://petstore.swagger.io/v2 | /pet/		  	| 100005			|	available 					 | sold  		  	 | doggie  | 200						 |


@Invalid_user_case
Scenario Outline: Updates a pet in the store with invalid petId form data ( POST /pet/{petId} )
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User update pet status to '<new_petStatus>' for '<invalid_petId>' and '<petName>'
Then Response should have status code '<405_status_code>'
And Response body should have error message '<invalid_input_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath | invalid_petId | new_petStatus | petName | invalid_input_error_msg | 405_status_code |
	|	https://petstore.swagger.io/v2 | /pet/		  	| 	001	  			| sold  		  	| doggie  | Invalid input 					| 405						  |



Scenario Outline: Deletes a pet from pet store (DELETE /pet/{petId} )
Given User has '<baseURI>' of Rest API with '/pet'
When User add a new pet to the store with '<valid_petId>' and '<petName>' and '<available_pet_status>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_petId>' and '<petName>' and '<available_pet_status>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User delete '<valid_petId>' Pet Id from petstore using '<api_key>'
Then Response should have status code '<200_status_code>'
Examples: 
	|	baseURI 					   					 | resourcePath |	valid_petId |	petName | available_pet_status | api_key | 200_status_code | 
	|	https://petstore.swagger.io/v2 | /pet/		  	|	100006			|	doggie 	| available 					 | 123  	 | 200  					 |








