Feature: Access to Petstore orders


Scenario Outline: Place an order for a pet with valid orderId data (POST /store/order)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User place order for purchasing the pet with '<valid_orderId>' and '<valid_petId>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_orderId>' and '<valid_petId>'
Examples: 
	|	baseURI 					   					 | resourcePath | valid_orderId| valid_petId | 200_status_code |
	|	https://petstore.swagger.io/v2 | /store/order | 10		  		 | 100007 		 | 200						 |

@Invalid_user_case
Scenario Outline: Place an order for a pet with invalid orderId data(POST /store/order)
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User place order for purchasing the pet with invalid order data
Then Response should have status code '<400_status_code>'
And Response body should have error message '<invalid_order_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath | invalid_order_error_msg  | 400_status_code |
	|	https://petstore.swagger.io/v2 | /store/order | No data									 | 400						 |


Scenario Outline: Find purchase order with valid orderId data (GET /store/order/{orderId})
Given User has '<baseURI>' of Rest API with '/store/order'
When User place order for purchasing the pet with '<valid_orderId>' and '<valid_petId>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_orderId>' and '<valid_petId>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User find purchase order by '<valid_orderId>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_orderId>' and '<valid_petId>'
Examples: 
	|	baseURI 					   					 | resourcePath 		  |	valid_orderId |	valid_petId | orderID_not_in_purchaseOrder | 200_status_code |
	|	https://petstore.swagger.io/v2 | /store/order/		  |	11			  		|	100008 			|	12	   	 			 	   					 | 200 						 |

@Invalid_user_case
Scenario Outline: Find purchase order with invalid orderId data (GET /store/order/{orderId})
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User find purchase order by '<invalid_orderId>'
Then Response should have status code '<400_status_code>'
And Response body should have error message '<invalid_ID_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath 		  |	invalid_orderId |	invalid_ID_error_msg | 400_status_code |
	|	https://petstore.swagger.io/v2 | /store/order/		  |		%			  			|	Invalid ID supplied	 | 400 						 |


Scenario Outline: Find non-existing purchase order with orderId data (GET /store/order/{orderId})
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User find purchase order by '<orderId_not_in_purchaseOrder>'
Then Response should have status code '<404_status_code>'
And Response body should have error message '<orderNotFound_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath 		  | orderId_not_in_purchaseOrder | 404_status_code | orderNotFound_error_msg |
	|	https://petstore.swagger.io/v2 | /store/order/		  |	000	   	 			 	   					 | 404 						 | Order not found				 |


Scenario Outline: Delete purchase order with valid orderId data (DELETE /store/order/{orderId})
Given User has '<baseURI>' of Rest API with '/store/order'
When User place order for purchasing the pet with '<valid_orderId>' and '<valid_petId>'
Then Response should have status code '<200_status_code>'
And Response body should have matching '<valid_orderId>' and '<valid_petId>'

Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User deletes the order with '<valid_orderId>'
Then Response should have status code '<200_status_code>'
And Response body should have deleted orderId '<valid_orderId>'
Examples: 

	|	baseURI 					   					 | resourcePath  | valid_orderId |	valid_petId | 200_status_code |
	|	https://petstore.swagger.io/v2 | /store/order/ | 12			  		 |	100009			| 200 						|

@Invalid_user_case
Scenario Outline: Delete purchase order by invalid orderId data (DELETE /store/order/{orderId})
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When  User deletes the order with '<invalid_orderId>'
Then Response should have status code '<400_status_code>'
And Response body should have error message '<invalid_ID_error_msg>'
Examples: 

	|	baseURI 					   					 | resourcePath  | invalid_orderId |	invalid_ID_error_msg | 400_status_code |
	|	https://petstore.swagger.io/v2 | /store/order/ | % 			  		 	 |	Invalid ID supplied	 | 400 						 |


Scenario Outline: Delete non-existing purchase order by orderId (DELETE /store/order/{orderId})
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When  User deletes the order with '<orderID_not_in_purchaseOrder>'
Then Response should have status code '<404_status_code>'
And Response body should have error message '<order_not_found_error_msg>'
Examples: 
	|	baseURI 					   					 | resourcePath  | orderID_not_in_purchaseOrder |	order_not_found_error_msg | 404_status_code |
	|	https://petstore.swagger.io/v2 | /store/order/ | 000			  		 							|	Order Not Found 					|	404 						|


@Invalid_user_case
Scenario Outline: Returns pet inventories by status (GET /store/inventory )
Given User has '<baseURI>' of Rest API with '<resourcePath>'
When User query pet inventory
Then Response should have status code '<200_status_code>'
Examples: 
	|	baseURI 					   					 | resourcePath 			  | 200_status_code | 
	|	https://petstore.swagger.io/v2 | /store/inventory		  | 200 	   				|

	
